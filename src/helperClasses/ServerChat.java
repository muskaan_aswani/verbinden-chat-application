package helperClasses;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.net.Socket;
public class ServerChat{
	public static HashMap<String,Socket> clients = new HashMap<>();
	public static  HashMap<String,Socket> getClients(){
		return clients;
	}
	public static void main(String args[])
	{
		try{
			ServerSocket server = new ServerSocket(9000);
			System.out.println("Server Started...");
			while(true){
				new Echoer(server.accept()).start();
			}
		}catch(IOException e){
			System.out.println("Issue : " +e.getMessage());
		}
	}
}