package helperClasses;

import com.mysql.jdbc.log.Log;
import controllers.ChatWindowController;
import controllers.LoginController;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.stage.Stage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pages.ChatWindow;

import java.io.*;
import java.net.Socket;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;


public class ClientChat  {

	Socket socket;
	PrintWriter output;
	Boolean flag = false;
	String receiverName = "";
	JSONObject msg;
	public static String userName;
	ChatWindowController c;


	public void createSocket(String clientName) throws IOException {
		socket = new Socket("localhost",9000) ;
		output = new PrintWriter(socket.getOutputStream(),true);
		output.println(clientName);
	}


	public void sendMessage(String message)  {
		String echoString = "";
		if(!flag)
		{
			receiverName = ChatWindowController.receiverName;
			output.println(ChatWindowController.receiverName);
			flag = true;
		}
		echoString = message;

		msg = new JSONObject();
		msg.put("sender", LoginController.userName);
		msg.put("receiver",ChatWindowController.toSendName);
		msg.put("content",echoString);
		msg.put("time",new Timestamp(System.currentTimeMillis()).toString());

		echoString=echoString + "/" +LoginController.userName+"$"+ChatWindowController.toSendName;

		output.println(echoString);// sends to server

		createChatsLog();
	}


	public void logout() throws IOException {
		output.println("logout&&"+ LoginController.userName);
		socket.close();
		System.exit(1);
	}


	private void createChatsLog(){
		String filename = "src\\archivedChats\\"+ChatWindowController.filename;
		try {

			if (ChatWindowController.file.exists()) {

				JSONParser parser = new JSONParser();
				Object obj1 =  parser.parse(new FileReader(filename));
				JSONObject newObj = (JSONObject) obj1;
				ChatWindowController.obj = newObj;
				ChatWindowController.jsonArrayMsgs = (JSONArray) newObj.get("messages");
			}

				ChatWindowController.jsonArrayMsgs.add(msg);
				ChatWindowController.obj.put("messages",ChatWindowController.jsonArrayMsgs);
				Files.write(Paths.get(filename), ChatWindowController.obj.toJSONString().getBytes());

		}catch (Exception e){
			System.out.println(e);
		}
	}

	public void getUserName(String name){
		userName = name;
	}

	public void connectToChat(ChatWindowController c) throws IOException {
		this.c = c;
		new MultiChatClient(this.socket,c,userName).start();
	}

}


class MultiChatClient extends Thread{
	Socket socket;
	ChatWindowController c;
	Parent root;
	String loggedInUserName;

	MultiChatClient(Socket socket, ChatWindowController c, String userName) throws IOException {
		this.socket = socket;
		this.c = c;
		loggedInUserName = userName;
	}



	public void run(){
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			while (true) {
				String str = input.readLine();
				System.out.println(str);
				String r = str.substring((str.lastIndexOf("/")+1),str.lastIndexOf("$"));
				String msg = str.substring(0,str.lastIndexOf("/"));
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								c.setIncomingMessage(msg,r);
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					});

			}
		}catch(Exception e){
			System.out.println("Exception "+ e);
		}
	}
}

