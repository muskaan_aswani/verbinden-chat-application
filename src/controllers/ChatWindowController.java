package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import helperClasses.Echoer;
import helperClasses.ServerChat;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ChatWindowController implements Initializable {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;


    @FXML
    private Label username;

    @FXML
    private Pane newChat;

    @FXML
    private ListView<String> addNewUserForChat;

    @FXML
    private ListView<String> optionsList;

    @FXML
    private ListView<String> chatOptions;


    @FXML
    private Pane leftHeader;

    @FXML
    private JFXTextField search;

    @FXML
    private Pane leftHeaderOpaque;

    @FXML
    private Pane rightOpaque;

    @FXML
    private Line partition;

    @FXML
    public ListView<String> usersList;

    @FXML
    public Label statusLabel;

    @FXML
    public Circle statusCircle;

    @FXML
    private Pane homeChatWindow;

    @FXML
    private Pane userChatWindow;

    @FXML
    public static Pane header;

    @FXML
    private Label toSend;

    @FXML
    private Pane changePasswordModal;

    @FXML
    private JFXPasswordField currentPassword;

    @FXML
    private JFXPasswordField newPassword;

    @FXML
    private JFXButton updatePassword;

    @FXML
    private Label incorrectOldPassword;

    @FXML
    private Label emptyPassword;

    @FXML
    private JFXTextArea txtMessage;

    @FXML
    private Pane passwordResetSuccess;

    @FXML
    private Label closeToaster;

    public static File file;
    public static JSONObject obj;
    public static JSONArray jsonArrayMsgs;
    public static String filename;
    public static String toSendName;
    public static String receiverName;

    @FXML
    public VBox messagesPane;

    @FXML
    public ScrollPane scroll;

    public ChatWindowController c = this;

    private boolean isConnected = false;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        optionsList.getItems().add("Log Out");
        optionsList.getItems().add("Reset Password");

        chatOptions.getItems().add("Clear Chat for everyone");


        username.setText(LoginController.userName);
        conn = db.MySQLConnect.connectionDB();
        initializeUsersList();
    }

    private void initializeUsersList(){
        usersList.getItems().clear();
        int id = LoginController.userId;
        String sql = "select * from users where id != " + id;

        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            addNewUserForChat.getItems().clear();
            while (rs.next()) {

                addNewUserForChat.getItems().add(rs.getString("username"));
            }
            File dir = new File("src\\archivedChats");
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    String fname = child.getName();
                    String name1 = fname.substring(0, fname.indexOf("_"));
                    String name2 = fname.substring(fname.indexOf("_") + 1, fname.length() - 5);
                    if (name1.equals(LoginController.userName)) {
                        usersList.getItems().add(name2);
                        addNewUserForChat.getItems().remove(name2);
                    } else if (name2.equals(LoginController.userName)) {
                        usersList.getItems().add(name1);
                        addNewUserForChat.getItems().remove(name1);
                    }
                }
            }


        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @FXML
    public void showUserChat(javafx.scene.input.MouseEvent mouseEvent) throws IOException, ParseException {

        homeChatWindow.setVisible(false);
        userChatWindow.setVisible(true);
        toSendName = usersList.getSelectionModel().getSelectedItem();
        toSend.setText(toSendName);
        usersList.getSelectionModel().clearSelection();
        if(toSendName == null){
            homeChatWindow.setVisible(true);
            userChatWindow.setVisible(false);

        }else {
            System.out.println(Echoer.onlineUsers2);

            if (Echoer.onlineUsers2.contains(toSendName)) {
                c.statusLabel.setText("online");
                c.statusCircle.setFill(Color.GREEN);
            } else {
                c.statusLabel.setText("");
                c.statusCircle.setFill(Color.TRANSPARENT);
            }
            System.out.println(ServerChat.getClients());
            //showStatus();
            txtMessage.requestFocus();

            scroll.vvalueProperty().bind(messagesPane.heightProperty());

            LoginController.obj.connectToChat(this);

            LoginController.obj.getUserName(LoginController.userName);

            try {
                if (toSendName.compareToIgnoreCase(LoginController.userName) < 0) {
                    filename = toSendName + "_" + LoginController.userName + ".json";
                } else {
                    filename = LoginController.userName + "_" + toSendName + ".json";
                }
                file = new File("src\\archivedChats\\" + filename);
                obj = new JSONObject();
                jsonArrayMsgs = new JSONArray();

            } catch (Exception e) {
                System.out.println(e);
            }
            getChatLogs();
        }
    }

    public void setIncomingMessage(String str, String receiver) throws IOException, ParseException {
        if (receiver.equalsIgnoreCase(ChatWindowController.toSendName)) {
            c.setCurrentChat(str, false);
            c.showStatus();
        }
    }

    @FXML
    public void sendMessage(ActionEvent actionEvent) throws IOException, ParseException {
        receiverName = usersList.getSelectionModel().getSelectedItem();
        LoginController.obj.sendMessage(txtMessage.getText());
        setCurrentChat(txtMessage.getText(), true);

        txtMessage.setText("");
    }

    public void setCurrentChat(String msg, Boolean b) {
        Label l2 = new Label(msg);
        l2.getStyleClass().add("messages");
        BorderPane b2 = new BorderPane();
        if (b) {
            l2.setMaxWidth(messagesPane.getWidth() / 2 + 50);
            l2.setWrapText(true);
            messagesPane.getChildren().add(b2);
            l2.getStyleClass().add("sender");
            b2.setRight(l2);
            b2.setBottom(new Label("Sent"));
        } else {
            b2.setLeft(l2);
            l2.getStyleClass().add("receiver");
            messagesPane.getChildren().add(b2);
        }
        scroll.vvalueProperty().bind(messagesPane.heightProperty());
    }

    public void getChatLogs() throws IOException, ParseException {

        if (messagesPane.getChildren().size() > 0) {
            messagesPane.getChildren().clear();
        }
        if (file.exists()) {
            JSONParser parser = new JSONParser();
            Object obj1 = parser.parse(new FileReader("src\\archivedChats\\" + filename));
            JSONObject newObj = (JSONObject) obj1;
            JSONArray arr = (JSONArray) newObj.get("messages");
            for (int i = 0; i < arr.size(); i++) {

                JSONObject arrObj = (JSONObject) arr.get(i);
                Label l2 = new Label((String) arrObj.get("content"));
                l2.getStyleClass().add("messages");

                if (arrObj.get("sender").equals(LoginController.userName)) {
                    l2.setMaxWidth(messagesPane.getWidth() / 2 + 50);
                    l2.setWrapText(true);
                    BorderPane b2 = new BorderPane();
                    messagesPane.getChildren().add(b2);
                    l2.getStyleClass().add("sender");
                    b2.setRight(l2);
                } else {
                    BorderPane b2 = new BorderPane();
                    messagesPane.getChildren().add(b2);
                    b2.setLeft(l2);
                    l2.getStyleClass().add("receiver");
                }
            }
        }
    }

    public void showUserList(MouseEvent mouseEvent) {
        addNewUserForChat.getSelectionModel().clearSelection();
        userChatWindow.setVisible(false);
        homeChatWindow.setVisible(true);
        leftHeaderOpaque.setVisible(true);
        rightOpaque.setVisible(true);
        newChat.setVisible(true);
        partition.toBack();
        newChat.toFront();

    }

    public void closeNewChat(MouseEvent mouseEvent) {
        hideNewChat();
    }

    public void addUserToChatList(MouseEvent mouseEvent) {
        toSendName = addNewUserForChat.getSelectionModel().getSelectedItem();
        usersList.getItems().add(toSendName);
        addNewUserForChat.getItems().remove(toSendName);
        hideNewChat();

    }

    public void hideNewChat() {
        usersList.getSelectionModel().clearSelection();
        newChat.setVisible(false);
        partition.toFront();
        leftHeaderOpaque.setVisible(false);
        rightOpaque.setVisible(false);
    }

    public void showOptionsList(MouseEvent mouseEvent) {
        if (optionsList.isVisible()) {
            optionsList.setVisible(false);
        } else {
            optionsList.toFront();
            optionsList.setVisible(true);
        }
    }


    public void performOptionSelected(MouseEvent mouseEvent) throws IOException {
        String option = optionsList.getSelectionModel().getSelectedItem();
        if (option.equals("Log Out")) {
            LoginController.obj.logout();
        }
        if (option.equals("Reset Password")) {
            rightOpaque.setVisible(true);
            leftHeaderOpaque.setVisible(true);
            userChatWindow.setVisible(false);
            homeChatWindow.setVisible(true);
            optionsList.setVisible(false);
            changePasswordModal.setVisible(true);
            changePasswordModal.toFront();
        }
    }


    public void showStatus() {
        c.statusLabel.setText("online");
        c.statusCircle.setFill(Color.GREEN);
    }

    public void resetPassword(ActionEvent actionEvent) {

        incorrectOldPassword.setVisible(false);
        emptyPassword.setVisible(false);

        String sql = "select * from users where username = '" + LoginController.userName+"'";

        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            String currentPasswordStr="";
//            System.out.println(currentPassword.getText());
            if(rs.next()) {
                currentPasswordStr = rs.getString("password");
            }
            if(newPassword.getText().isEmpty()){
                emptyPassword.setVisible(true);
            }
            else {

                System.out.println(currentPasswordStr);
                System.out.println(RegisterController.generateHash(currentPassword.getText()));

                if (currentPasswordStr.equals(RegisterController.generateHash(currentPassword.getText()))) {
                    String newPasswordStr = newPassword.getText();
                    sql = "update users set password = '" + RegisterController.generateHash(newPassword.getText()) + "' where username = '" + LoginController.userName+"'";
                    ps = conn.prepareStatement(sql);
                    ps.execute();
                    changePasswordModal.setVisible(false);
                    passwordResetSuccess.setVisible(true);
                    leftHeaderOpaque.setVisible(false);
                    rightOpaque.setVisible(false);
                } else {
                    System.out.println("ullu banaya bada maza aya");
                    incorrectOldPassword.setVisible(true);
                }
            }

        } catch (SQLException | NoSuchAlgorithmException throwables) {
            throwables.printStackTrace();
        }

        currentPassword.setText("");
        newPassword.setText("");

    }

    public void hideToaster(MouseEvent mouseEvent) {
        passwordResetSuccess.setVisible(false);
    }

    public void performChatOptionSelected(MouseEvent mouseEvent) throws IOException, ParseException {
        String option = chatOptions.getSelectionModel().getSelectedItem();
        if (option.equals("Clear Chat for everyone")) {

            FileWriter fwOb = new FileWriter("src\\archivedChats\\" + filename, false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            fwOb.write("{\"messages\":[]}");
            pwOb.close();
            fwOb.close();
            chatOptions.setVisible(false);
            getChatLogs();
        }
    }

    public void showOptionsList2(MouseEvent mouseEvent) {
        if (chatOptions.isVisible()) {
            chatOptions.setVisible(false);
        } else {
            chatOptions.toFront();
            chatOptions.setVisible(true);
        }
    }

    public void searchUser(KeyEvent keyEvent) throws SQLException {

        String s = search.getText();
        if(s.isEmpty()){
            initializeUsersList();
        }else {
            String sql = "select * from users where username like '%" + s + "%' and username != '" + LoginController.userName + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            refreshList(rs);
        }

    }


    private void refreshList(ResultSet rs) throws SQLException {
        usersList.getItems().clear();
        while (rs.next()) {
            usersList.getItems().add(rs.getString("username"));
        }
    }

}



