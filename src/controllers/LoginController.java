package controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import helperClasses.ClientChat;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginController {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    static ClientChat obj;
    public static int userId;
    public static String userName;


    @FXML
    private Button btnLogin;

    @FXML
    private Button btnRegister;

    @FXML
    private JFXTextField txtUsername;

    @FXML
    private Label errorNote;


    @FXML
    private JFXPasswordField txtPassword;


    @FXML
    void login(ActionEvent event) throws NoSuchAlgorithmException, SQLException, IOException {
        conn = db.MySQLConnect.connectionDB();
        String username = txtUsername.getText();
        String password = RegisterController.generateHash(txtPassword.getText());

        String sql = "select * from users where username = ? and password = ?";

        ps = conn.prepareStatement(sql);
        ps.setString(1,username);
        ps.setString(2,password);

        rs = ps.executeQuery();



        if(rs.next()){
            userId = rs.getInt("id");
            userName = rs.getString("username");
            System.out.println("Logged in" + userName);
            obj = new ClientChat();
            obj.createSocket(username);
            btnLogin.getScene().getWindow().hide();

            Stage main = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("../fxmls/chatWindow.fxml"));
            main.setScene(new Scene(root,900,700));
            main.show();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxmls/chatWindow.fxmll"));

        }
        else{
            errorNote.setVisible(true);
        }
        
    }




    @FXML
    void redirectToRegister(ActionEvent event) throws IOException {
        btnRegister.getScene().getWindow().hide();
        Stage main = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxmls/registerUser.fxml"));
        main.setScene(new Scene(root,900,700));
        main.show();
    }

}
