package controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {
    Connection conn;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public static String generateHash(String input) throws NoSuchAlgorithmException {
        StringBuilder hash = new StringBuilder();

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f' };
            for (int idx = 0; idx < hashedBytes.length; idx++) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }

        return hash.toString();
    }


    @FXML
    private JFXTextField txtEmail;

    @FXML
    private Button btnRegister;

    @FXML
    private Button btnLogin;

    @FXML
    private JFXTextField txtUsername;

    @FXML
    private JFXPasswordField txtPassword;

    @FXML
    private Label errorNote;

    @FXML
    void redirectToLogin(ActionEvent event) throws IOException {
        btnLogin.getScene().getWindow().hide();
        Stage main = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../fxmls/login.fxml"));
        main.setScene(new Scene(root,900,700));
        main.show();
    }

    @FXML
    void registerUser(ActionEvent event) throws NoSuchAlgorithmException {
        String email = txtEmail.getText();
        String username = txtUsername.getText();
        String password = generateHash(txtPassword.getText());

            if(!email.isEmpty() && !username.isEmpty() && !password.isEmpty()){
                String sql= "Insert into users(username,password,email) values(?,?,?)";
                try{
                    ps = conn.prepareStatement(sql);
                    ps.setString(1,username);
                    ps.setString(2,password);
                    ps.setString(3,email);

                    ps.execute();
                    System.out.println("Inserted");
                    btnRegister.getScene().getWindow().hide();
                    Stage main = new Stage();
                    Parent root = FXMLLoader.load(getClass().getResource("../sample/sample.fxml"));
                    main.setScene(new Scene(root,900,700));
                    main.show();
                }catch (SQLException | IOException e){
                    System.out.println(e);
                }
            }else{
                errorNote.setVisible(true);
            }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        conn = db.MySQLConnect.connectionDB();
    }


}
