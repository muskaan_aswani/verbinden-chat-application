package db;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class MySQLConnect {
    Connection conn;

    public static Connection connectionDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat_system","root","");
//            JOptionPane.showMessageDialog(null,"Connection established successfully!");
            System.out.println("Connection established Successfully!");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Connection Failed!" + e);
            return null;
        }
    }
}
