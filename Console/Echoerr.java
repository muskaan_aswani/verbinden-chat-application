import java.net.*;
import java.io.*;

public class Echoerr extends Thread
{
	private Socket clientSocket;
	public Echoerr(Socket clientSocket)
	{
		this.clientSocket = clientSocket;
	}
	public void run()
	{
		try
		{
			BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			
			while(true)
			{
				String echoString = input.readLine();
				if(echoString.equals("exit"))
				{
					break;
				}
				System.out.println(echoString);
				try
				{
					Thread.sleep(1);
				}
				catch(InterruptedException e)
				{
					System.out.println("Interrupted : "+e.getMessage());
				}
				//new Input1(clientSocket).start();
			}
		}
		catch(IOException e)
		{
			System.out.println("Issue : " + e);
		}
		finally		
		{
			try
			{
				clientSocket.close();
			}
			catch(IOException e)
			{
				System.out.println("Issue : " + e.getMessage());
			}
		}
	}
}
