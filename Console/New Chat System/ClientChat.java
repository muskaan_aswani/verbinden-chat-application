

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;
import java.util.*;

public class ClientChat  {
	public static void main(String args[])
	{
		try{ 
			Socket socket = new Socket("localhost",9000) ;
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
			Scanner scanner = new Scanner(System.in);
			String echoString = "";
			String response;
			String toSend;
			String name = "";
			while(name.isEmpty()){
				System.out.println("Enter name : ");
				name =  scanner.nextLine();
				output.println(name);
			}
			System.out.println("Do you want to chat?");
			String ans = scanner.nextLine();
			if(ans.equalsIgnoreCase("yes") || ans.equalsIgnoreCase("Y")){
				System.out.println("Enter name of other client ");
				toSend = scanner.nextLine();
				output.println(toSend);
				do{

					new MultiChatClient(socket).start();
					echoString = scanner.nextLine();
					output.println(echoString);// sends to server

						/*if(!echoString.equals("exit")){
						response = input.readLine();
						System.out.println("Server: "+ response);}*/
					
						
				}while(!echoString.equals("exit"));	
			}
			else {
				toSend = "no";
				while(true){
					new MultiChatClient(socket).start();
				}
			}
			
			
			
		
		}
		catch(SocketTimeoutException e){
			System.out.println("Issue: "+e.getMessage());
		}catch(IOException e){
			System.out.println("Issue: "+e.getMessage());
		}
		}
		
}
class MultiChatClient extends Thread{
	Socket socket;
	MultiChatClient(Socket socket){
		this.socket = socket;
	}
	public void run(){
	try{
		BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		while(true)
		{
			String str = input.readLine();
			System.out.println(str);
		}

	}catch(Exception e){

	}
}
}