import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.net.Socket;
public class ServerChat{
	static HashMap<String,Socket> clients = new HashMap<>();
	
	public static void main(String args[])
	{
		try{
			ServerSocket server = new ServerSocket(9000);
			while(true){
				new Echoer(server.accept()).start();
			}
		}catch(IOException e){
			System.out.println("Issue : " +e.getMessage());
		}
	}
}