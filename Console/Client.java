import java.net.Socket;
import java.io.*;
import java.util.*;
import java.net.*;
public class Client
{
	public static void main(String args[])
	{
		try
		{
			Socket socket = new Socket("localhost", 9700);
			//socket.setSoTimeout(5000);
			
			PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
			Scanner scanner = new Scanner(System.in);
			String echoString;
			String response;
			new Input(socket).start();
			do
			{
				System.out.println("Enter the String to be sent to the server :");
				echoString = scanner.nextLine();
				output.println("Client : "+echoString);
			}
			while(!echoString.equals("exit"));
		}
		catch(SocketTimeoutException e)
		{

		}

		
		catch(IOException ioe)
		{

		}	
	}

}
class Input extends Thread
{
	Socket socket;
	Input(Socket socket)
	{
		this.socket = socket;
		
	}
	public void run()
	{
		try
		{
			while(true)
			{
				BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String response = input.readLine();
				output.println("Client: " + response);
			}
		}
		catch(Exception e)
		{
			
		}
	}
}	