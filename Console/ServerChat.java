import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.net.Socket;
public class ServerChat{
	static HashMap<String,Socket> clients = new HashMap<>();
	public HashMap<String ,Socket> getClients(){
		return this.clients;
	}
	public static void main(String args[])
	{
		

		try{
			ServerSocket server = new ServerSocket(9000);
			while(true){
				/*Socket client = server.accept();
				Echoer echoer = new Echoer(client);
				echoer.start();*/
				new Echoer(server.accept()).start();
			}
		}catch(IOException e){
			System.out.println("Issue : " +e.getMessage());
		}
	}
}