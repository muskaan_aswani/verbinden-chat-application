import java.net.*;
import java.io.*;

public class Echoerr1 extends Thread
{
	private Socket clientSocket;
	public Echoerr1(Socket clientSocket)
	{
		this.clientSocket = clientSocket;
	}
	public void run()
	{
		try
		{
			PrintWriter output = new PrintWriter(this.clientSocket.getOutputStream() , true);
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			
			while(true)
			{
				if(!output.equals("exit"));
				
					//System.out.println("Enter msg: ");
					String str = input.readLine();
					if(str != null)
					output.println("Client : " + str);
				}

			
		}
		catch(IOException ioe)
		{

		}
		finally		
		{
			try
			{
				clientSocket.close();
			}
			catch(IOException e)
			{
				System.out.println("Issue : " + e.getMessage());
			}
		}
	}

}